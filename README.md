# Welcome
## Repository Structure

- viz_gallery : put all static html for visualization purposes there
- AzureFunction : where all currently running azure function

## How to Deploy Azure function
First make sure to setup azure-cli in your local computer : https://docs.microsoft.com/en-us/cli/azure/install-azure-cli?view=azure-cli-latest

1. Go to function directory (i.e. `kc19-statistic-harian2`)
2. Run following command : `func azure functionapp publish <function-app-name>`
3. See the status in azure console : <a href="https://portal.azure.com/#blade/WebsitesExtension/FunctionsIFrameBlade/id/%2Fsubscriptions%2F3de5e3cc-db2c-4033-b411-38453fed6e9d%2FresourceGroups%2Fkawalcovid19%2Fproviders%2FMicrosoft.Web%2Fsites%2Fkc19-statistik-harian2">click here</a>