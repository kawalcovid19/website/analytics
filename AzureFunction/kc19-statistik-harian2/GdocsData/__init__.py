import logging

import azure.functions as func

from azure.storage.blob import (
    BlockBlobService
)
import pandas as pd
import numpy as np
import requests
import os
import io


def main(mytimer: func.TimerRequest) -> None:
    ## Load data from gsheets
    # ## Load data from gsheets
    url = ("https://docs.google.com/spreadsheets/d/1ma1T9hWbec1pXlwZ89WakRk-OfVUQZsOCFl4FwZxzVw/htmlview")
    dfs = pd.read_html(url, skiprows=0)
    dfs = [df for df in dfs if "B" in df.columns]

    # df = dfs[0]
    # col = list(df.iloc[1])
    # df.columns = col
    # df = df.iloc[3:, :]
    # df = df.iloc[:, 1:]
    # df = df.loc[:, :"Status"]
    # df = df[df.No.notnull()]

    df_daily = [df for df in dfs if "Kasus baru" in list(df.B)][0]
    df_prov = [df for df in dfs if "Provinsi Asal" in list(df.B)][0]

    ## Processing for Statistik Harian
    # sheet_no = [i for i, df in enumerate(dfs) if "sembuh" in str(df).lower() and "baru" in str(df).lower()][0]
    # df_daily = dfs[sheet_no]
    col = list(df_daily.iloc[0])
    df_daily.columns = col
    df_daily = df_daily.iloc[1:, 1:]
    df_daily = df_daily.loc[:, :"Catatan"]
    df_daily = df_daily[df_daily.iloc[:, 3].notnull()]

    ## Processing for Case by Province
    # sheet_no = [i for i,df in enumerate(dfs) if "provinsi" in str(df).lower() and "asal" in str(df).lower()][0]
    # df_prov = dfs[sheet_no]
    col = list(df_prov.iloc[1])
    df_prov.columns = col
    df_prov = df_prov.iloc[2:, 1:]
    # df_prov = df_prov.loc[:, :"Sembuh"]
    df_prov = df_prov[df_prov.iloc[:, 1].notnull()]

    # Upload to Azure Storage (Full Data)
    accountName = "kawalcovid19"
    accountKey = os.environ["AZURE_BLOB_KAWALCOVID19_KEY"]
    containerName = "analytics-dataset"

    blobService = BlockBlobService(account_name=accountName, account_key=accountKey)
    # blobService.create_blob_from_text(containerName, "data/raw/daftar_positif.csv",
    #                                   df.to_csv(index=False, encoding="utf-8"))
    blobService.create_blob_from_text(containerName, "data/raw/statistik_harian.csv",
                                      df_daily.to_csv(index=False, encoding="utf-8"))
    blobService.create_blob_from_text(containerName, "data/raw/kasus_provinsi.csv",
                                      df_prov.to_csv(index=False, encoding="utf-8"))

    # r = requests.get("http://covid-monitoring2.kemkes.go.id/")
    # df_nodes = pd.DataFrame(r.json()["nodes"])
    # df_link = pd.DataFrame(r.json()["links"])
    # df_nodes = pd.merge(df_nodes, df_link, left_on="id", right_on="source", how="left")
    # df_nodes = df_nodes.rename(columns={
    #     "kasus": "No", "umur": "Usia", "gender": "JK", "wn": "WN", "klaster": "Provinsi",
    #     "status": "Status"
    # })
    # df_nodes.loc[:, "source"] = df_nodes.source.apply(lambda x: np.nan if str(x) == "nan" else str(int(x)))
    # df_nodes.loc[:, "target"] = df_nodes.target.apply(lambda x: np.nan if str(x) == "nan" else str(int(x)))
    # df_nodes.loc[:, "JK"] = df_nodes["JK"].apply(lambda x: x[0])
    #
    # blobService = BlockBlobService(account_name=accountName, account_key=accountKey)
    # blobService.create_blob_from_text(containerName, "data/raw/daftar_positif_kemenkes.csv",
    #                                   df_nodes.to_csv(index=False, encoding="utf-8"))

