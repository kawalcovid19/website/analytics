import logging

import azure.functions as func

from azure.storage.blob import (
    BlockBlobService
)
import pandas as pd
import os
import io


def main(mytimer: func.TimerRequest) -> None:
    ## Load data from gsheets
    url = ("https://docs.google.com/spreadsheets/d/1avGWWl1J19O_Zm0NGTGy2E-fOG05i4ljRfjl87P7FiA/"
           "htmlview?ts=5e5e9222&sle=true#gid=0")
    df = pd.read_html(url)
    df = df[0]
    new_header = df.iloc[0]
    df = df[1:]
    df.columns = new_header
    df = df.iloc[:, 1:]
    df = df[
        ["Country_Region", "Province_State", "Date", "Case_Type", "Cases", "Lat", "Long", "Difference", "Latest_Date"]]
    df = df.rename(columns={"Country_Region": "Country/Region", "Province_State": "Province/State",
                            "Latest_Date": "Last_Update_Date"})

    # Upload to Azure Storage (Full Data)
    accountName = "kawalcovid19"
    accountKey = os.environ["AZURE_BLOB_KAWALCOVID19_KEY"]
    containerName = "analytics-dataset"
    blobName = "data/raw/covid_all_country.csv"

    blobService = BlockBlobService(account_name=accountName, account_key=accountKey)
    blobService.create_blob_from_text(containerName, blobName, df.to_csv(index=False, encoding="utf-8"))

    # Upload to Azure Storage (Selected Country)
    country_to_pick = ["Japan", "France", "Singapore", "Malaysia", "Thailand", "Philippines", "Vietnam", "Australia",
                       "Indonesia", "Iran", "Korea, South"]
    df_mini = df[df["Country/Region"].isin(country_to_pick)]

    containerName = "analytics-dataset"
    blobName = "data/raw/covid_selected_country.csv"

    blobService = BlockBlobService(account_name=accountName, account_key=accountKey)
    blobService.create_blob_from_text(containerName, blobName, df_mini.to_csv(index=False, encoding="utf-8"))
