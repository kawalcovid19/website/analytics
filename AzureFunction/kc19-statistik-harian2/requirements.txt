azure-functions
requests
azure-storage
pandas==0.25.1
lxml==4.4.1
html5lib==1.0.1
numpy==1.17.2