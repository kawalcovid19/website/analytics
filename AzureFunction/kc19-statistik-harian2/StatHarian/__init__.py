import logging
import azure.functions as func
import requests
import json
import os

PBIE_CLIENT_ID = os.environ["PBIE_CLIENT_ID"]
PBIE_USERNAME = "analytics@ainunnajibgmail.onmicrosoft.com"
PBIE_PASSWORD = os.environ["PBIE_ACC_PASSWORD"]
PBIE_GROUP_ID = "49468b5a-6a98-45c9-93be-602e91db6e4f"
PBIE_REPORT_ID  = "d3faeae4-427d-4725-9c41-ae81a0c647b1"


class EmbedToken:
    def __init__(self, report_id, group_id, settings=None):
        self.username = PBIE_USERNAME
        self.password = PBIE_PASSWORD
        self.client_id = PBIE_CLIENT_ID
        self.report_id = report_id
        self.group_id = group_id
        if settings is None:
            self.settings = {'accessLevel': 'View', 'allowSaveAs': 'false'}
        else:
            self.settings = settings
        self.access_token = self.get_access_token()
        self.config = self.get_embed_token()
        self.json = json.dumps(self.config)

    def get_access_token(self):
        data = {
            'grant_type': 'password',
            'scope': 'openid',
            'resource': r'https://analysis.windows.net/powerbi/api',
            'client_id': self.client_id,
            'username': self.username,
            'password': self.password
        }
        response = requests.post('https://login.microsoftonline.com/common/oauth2/token', data=data)
        return response.json().get('access_token')

    def get_embed_token(self):
        dest = 'https://api.powerbi.com/v1.0/myorg/groups/' + self.group_id \
               + '/reports/' + self.report_id + '/GenerateToken'
        embed_url = 'https://app.powerbi.com/reportEmbed?reportId=' \
                    + self.report_id + '&groupId=' + self.group_id
        headers = {'Authorization': 'Bearer ' + self.access_token}
        response = requests.post(dest, data=self.settings, headers=headers)
        self.token = response.json().get('token')
        return {'EmbedToken': self.token, 'EmbedUrl': embed_url, 'ReportId': self.report_id}


def main(req: func.HttpRequest) -> func.HttpResponse:
    logging.info('Python HTTP trigger function processed a request.')
    token = EmbedToken(PBIE_REPORT_ID, PBIE_GROUP_ID)
    return func.HttpResponse(
        "callback("+token.json+")",
        status_code=200,
        mimetype='application/json'
    )
